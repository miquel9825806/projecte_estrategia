using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ShopManager : MonoBehaviour
{
    public GameObject Player;
    public GameObject Nexus;
    private GameObject ShopObject;
    public List<Upgrade> UpgradesList;

    public Image Image1;
    public TextMeshProUGUI Desc1;
    public Button Button1;
    
    public Image Image2;
    public TextMeshProUGUI Desc2;
    public Button Button2;

    public Image Image3;
    public TextMeshProUGUI Desc3;
    public Button Button3;

    private AudioSource audioSource;
    [SerializeField] public AudioClip shopItem;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
        }
        audioSource.playOnAwake = false;
        audioSource.clip = shopItem;
        audioSource.Stop();
    }

    void OnEnable()
    {
        ShopObject = GameObject.Find("Shop");
        GenerateUpgrades();
    }

    public void GenerateUpgrades()
    {
        List<int> ChosenUpgrades = new List<int> { Random.Range(0, UpgradesList.Count), Random.Range(0, UpgradesList.Count), Random.Range(0, UpgradesList.Count) };
        int i = 0;
        while (i < 3)
        {
            if (ChosenUpgrades[i] == 0 || ChosenUpgrades[i] == 1 || ChosenUpgrades[i] == 2 || ChosenUpgrades[i] == 4 || ChosenUpgrades[i] == 7)
            {
                i++;
            }
            else
            {
                if (ChosenUpgrades[i] == 3)
                {
                    if (Player.GetComponent<PlayerController>().CritChance >= 50)
                    {
                        ChosenUpgrades[i] = Random.Range(0, UpgradesList.Count);
                    }
                    else
                    {
                        i++;
                    }
                }
                else if (ChosenUpgrades[i] == 5)
                {
                    if (Player.GetComponent<PlayerController>().MaxAttackCD <= 0)
                    {
                        ChosenUpgrades[i] = Random.Range(0, UpgradesList.Count);
                    }
                    else
                    {
                        i++;
                    }
                }
                else if (ChosenUpgrades[i] == 6)
                {
                    if (Player.GetComponent<PlayerController>().MaxSpeed >= 10)
                    {
                        ChosenUpgrades[i] = Random.Range(0, UpgradesList.Count);
                    }
                    else
                    {
                        i++;
                    }
                }
            }
        }

        Image1.sprite = UpgradesList[ChosenUpgrades[0]].UpgradeSprite;
        Desc1.text = UpgradesList[ChosenUpgrades[0]].EffectText;

        Image2.sprite = UpgradesList[ChosenUpgrades[1]].UpgradeSprite;
        Desc2.text = UpgradesList[ChosenUpgrades[1]].EffectText;

        Image3.sprite = UpgradesList[ChosenUpgrades[2]].UpgradeSprite;
        Desc3.text = UpgradesList[ChosenUpgrades[2]].EffectText;
    }

    public void ButtonEffect1()
    {
        audioSource.PlayOneShot(shopItem);
        ApplyUpgrade(Desc1.text);
        Time.timeScale = 1;
        ShopObject.SetActive(false);
    }

    public void ButtonEffect2()
    {
        audioSource.PlayOneShot(shopItem);
        ApplyUpgrade(Desc2.text);
        Time.timeScale = 1;
        ShopObject.SetActive(false);
    }

    public void ButtonEffect3()
    {
        audioSource.PlayOneShot(shopItem);
        ApplyUpgrade(Desc3.text);
        Time.timeScale = 1;
        ShopObject.SetActive(false);
    }

    private void ApplyUpgrade(string effectText)
    {
        if (effectText == UpgradesList[0].EffectText)
        {
            IncreaseDMG();
        }
        else if (effectText == UpgradesList[1].EffectText)
        {
            IncreaseHP();
        }
        else if (effectText == UpgradesList[2].EffectText)
        {
            IncreaseCritDamage();
        }
        else if (effectText == UpgradesList[3].EffectText)
        {
            IncreaseCritChance();
        }
        else if (effectText == UpgradesList[4].EffectText)
        {
            PlayerHealHP();
        }
        else if (effectText == UpgradesList[5].EffectText)
        {
            LowerAttackCD();
        }
        else if (effectText == UpgradesList[6].EffectText)
        {
            IncreaseSpeed();
        }
        else if (effectText == UpgradesList[7].EffectText)
        {
            IncreaseNexus();
        }
    }

    void IncreaseNexus()
    {
        Nexus.GetComponent<NexusController>().MaxHP += 5;
        Nexus.GetComponent<NexusController>().HP += 5;
        if (Nexus.GetComponent<NexusController>().HP <= Nexus.GetComponent<NexusController>().MaxHP / 2)
        {
            Nexus.GetComponent<NexusController>().HP += Nexus.GetComponent<NexusController>().MaxHP / 3;
        }
    }

    void IncreaseSpeed()
    {
        Player.GetComponent<PlayerController>().MaxSpeed++;
        Player.GetComponent<PlayerController>().Speed++;
    }

    void LowerAttackCD()
    {
        Player.GetComponent<PlayerController>().AttackCD -= 0.10f;
        Player.GetComponent<PlayerController>().MaxAttackCD -= 0.10f;
    }

    void PlayerHealHP()
    {
        Player.GetComponent<PlayerController>().HealHP(0);
    }

    void IncreaseDMG()
    {
        Player.GetComponent<PlayerController>().MaxDmg++;
        Player.GetComponent<PlayerController>().Dmg++;
    }

    void IncreaseHP()
    {
        Player.GetComponent<PlayerController>().MaxHP++;
        Player.GetComponent<PlayerController>().HealHP(1);
    }

    void IncreaseCritDamage()
    {
        Player.GetComponent<PlayerController>().CritDamage += 0.5f;
    }

    void IncreaseCritChance()
    {
        Player.GetComponent<PlayerController>().CritChance += 2;
    }
}
