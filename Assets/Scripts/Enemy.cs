using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Enemy : ScriptableObject
{
    public Sprite Sprite;
    public float Size;
    public int MaxSpeed;
    public int MaxHP;
    public int MaxDMG;
    public float MaxAttackSpeed;
    public GameObject Drop;
    public int Points;
    public AudioClip deathSound;
}
