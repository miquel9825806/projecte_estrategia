using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class GoldController : MonoBehaviour
{
    public GameObject Player;
    public GameObject GameManager;
    
    void Start()
    {
        Player.GetComponent<PlayerController>().AddingGold += ReloadGold;
        ReloadGold();
    }

    public void ReloadGold()
    {
        int NextShop = GameManager.GetComponent<GameManager>().RequiredGold;
        int Gold = Player.GetComponent<PlayerController>().Gold;
        this.GetComponent<TextMeshProUGUI>().text = Gold+" / "+NextShop;
    }
}
