using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AudioManager : MonoBehaviour
{
    public AudioClip[] Clips;
    private AudioSource AudioSource;
    [SerializeField] private TMP_Dropdown dropdown;
    private int Value = -1;
    
    void Start()
    {
        AudioSource = GetComponent<AudioSource>();
    }
    
    void Update()
    {
        if(!AudioSource.isPlaying && Value >= 0)
        {
            AudioSource.clip = Clips[Value];
            AudioSource.Play();
        }
    }

    void Change()
    {
        if(Value >= 0 && Value < Clips.Length)
        {
            AudioSource.clip = Clips[Value];
            AudioSource.Play();
        }
        else {
            AudioSource.Stop();
        }
    }
    
    public void GetDropdown()
    {
        Value = dropdown.value - 1;
        Change();
    }
}
