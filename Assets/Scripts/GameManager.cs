using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public GameObject Player;
    public GameObject PauseMenu;
    public GameObject ErrorText;
    public GameObject ShopObject;
    public int RequiredGold = 1;

    void Update()
    {
        if(!ShopObject.activeSelf){
            if (Input.GetKeyUp(KeyCode.Escape)){
                if (!PauseMenu.activeSelf) { 
                    PauseMenu.SetActive(true);
                    Time.timeScale = 0;
                }
                else
                {
                    PauseMenu.SetActive(false);
                    ErrorText.SetActive(false);
                    Time.timeScale = 1;
                }
            }
        }
    }

    public void Shop(){

        if(Player.GetComponent<PlayerController>().Gold >= RequiredGold){
            int PlayerGold = RequiredGold;
            if (RequiredGold < 1000000)
            {
                RequiredGold += Random.Range(1, 1 + RequiredGold / 2);
                if (RequiredGold > 1000000)
                {
                    RequiredGold = 1000000;
                }
            }
            Player.GetComponent<PlayerController>().AddGold(-PlayerGold);
            ErrorText.SetActive(false);
            PauseMenu.SetActive(false);
            ShopObject.SetActive(true);
        }else{
            ErrorText.SetActive(true);
        }
    }
    public void ResumeGame()
    {
        Time.timeScale = 1;
        ErrorText.SetActive(false);
        PauseMenu.SetActive(false);
        
    }
    public void GoToTitleScreen()
    {
        SceneManager.LoadScene("TitleScreen");
        Time.timeScale = 1;
    }
}