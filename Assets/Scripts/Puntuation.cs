using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Puntuation : ScriptableObject
{
    public int Points;
    public int Wave;
}
