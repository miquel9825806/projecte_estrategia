using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemy : EnemyClass
{

    protected override void Update()
    {
        Vector2 Direction = Objective - this.transform.position;
        rb.velocity = new Vector2(Direction.x, Direction.y).normalized * Speed;
    }
}
