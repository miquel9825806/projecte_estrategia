using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : EnemyClass
{

    protected override void Update()
    {
        Vector2 Direction = Objective - this.transform.position;
        rb.velocity = new Vector2(Direction.x, Direction.y).normalized * Speed;
    }
    
    protected virtual void OnCollisionEnter2D(Collision2D collision)
    {
    	if (collision.collider.CompareTag("Nexus"))
        {
            if (!OnCD)
            {
                collision.gameObject.GetComponent<NexusController>().TakeDamage(DMG);
                Point.Points += Object.Points;
                Destroy(this.gameObject);
            }
        }
        if (collision.collider.CompareTag("Player"))
        {
            if (!OnCD)
            {
                collision.gameObject.GetComponent<PlayerController>().HealHP(-DMG);
                Point.Points += Object.Points;
                Destroy(this.gameObject);
            }
        }
    }
}
