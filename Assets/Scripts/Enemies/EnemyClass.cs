using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public abstract class EnemyClass : MonoBehaviour
{
    public Enemy Object;
    public Puntuation Point;
    public int HP;
    public int Speed;
    public int DMG;
    public float AttackSpeed;
    public bool OnCD = false;
    public Rigidbody2D rb;
    public GameObject Player;
    public Vector3 Objective;
    public int valx = 1;
    public int valy = 1;
    protected Slider slider;

    protected virtual void Start()
    {
        SpriteRenderer Sprite = GetComponent<SpriteRenderer>();
        Sprite.sprite = Object.Sprite;
        rb = GetComponent<Rigidbody2D>();
        Player = GameObject.FindGameObjectWithTag("Player");
        Objective = GameObject.FindGameObjectWithTag("Nexus").transform.position;
        Speed = Object.MaxSpeed + (Point.Wave / 50);
        HP = Object.MaxHP + (Point.Wave / 5);
        DMG = Object.MaxDMG + (Point.Wave / 10);
        AttackSpeed = Object.MaxAttackSpeed;
        this.transform.localScale = new Vector3(Object.Size, Object.Size, 1);
    }

    protected abstract void Update();

    protected virtual void OnTriggerEnter2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("PlayerAttack"))
        {
            HP -= Player.GetComponent<PlayerController>().AttackDamage();
            if (HP <= 0)
            {
                if (Object.deathSound != null)
                {
                    AudioSource.PlayClipAtPoint(Object.deathSound, transform.position);
                }
                Instantiate(Object.Drop, transform.position, transform.rotation);
                Point.Points += Object.Points;
                Destroy(this.gameObject);
            }
        }
    }

    protected virtual void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.collider.CompareTag("Nexus"))
        {
            if (!OnCD)
            {
                collision.gameObject.GetComponent<NexusController>().TakeDamage(DMG);
                StartCoroutine(AttackCD());
            }
        }
        if (collision.collider.CompareTag("Player"))
        {
            if (!OnCD)
            {
                collision.gameObject.GetComponent<PlayerController>().HealHP(-DMG);
                StartCoroutine(AttackCD());
            }
        }
    }

    protected virtual IEnumerator AttackCD()
    {
        OnCD = true;
        yield return new WaitForSeconds(AttackSpeed);
        OnCD = false;
    }
}
