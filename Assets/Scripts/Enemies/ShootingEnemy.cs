using System.Collections;
using UnityEngine;

public class ShootingEnemy : EnemyClass
{
    public GameObject missile;

	
	protected override void Start()
	{
        SpriteRenderer Sprite = GetComponent<SpriteRenderer>();
        Sprite.sprite = Object.Sprite;
        rb = GetComponent<Rigidbody2D>();
        Player = GameObject.FindGameObjectWithTag("Player");
        Speed = Object.MaxSpeed + (Point.Wave/50);
        HP = Object.MaxHP + (Point.Wave / 5);
        DMG = Object.MaxDMG + (Point.Wave / 10);
        AttackSpeed = Object.MaxAttackSpeed;
        this.transform.localScale = new Vector3(Object.Size, Object.Size, 1);
        if(transform.position.x < 0){
        	valx=-1;
        }
        if(transform.position.y < 0){
        	valy=-1;
        }
        Objective = new Vector3((Speed * 2)*valx, (Speed * 2)*valy,0);
    }

    protected override void Update()
    {
    	if ((Objective.x >= this.transform.position.x-1 && Objective.x <= this.transform.position.x+1) && (Objective.y >= this.transform.position.y-1 && Objective.y <= this.transform.position.y+1)){
        	    rb.velocity = Vector2.zero;
        	    LaunchMissile();
        }
        else{
        	Vector2 direction = Objective - transform.position;
        	rb.velocity = direction.normalized * Speed;
        }
    }

    void LaunchMissile()
    {
        if (!OnCD)
        {
                Vector2 Dir = (Objective - transform.position).normalized;
                GameObject Bullet = Instantiate(missile, transform.position, transform.rotation);
                float angle = Mathf.Atan2(Dir.y, Dir.x) * Mathf.Rad2Deg;
                Bullet.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle));
                StartCoroutine(AttackCD());
        }
    }
}
