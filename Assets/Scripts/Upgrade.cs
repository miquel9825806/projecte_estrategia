using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class Upgrade : ScriptableObject
{
    public string EffectText;
    
    public Sprite UpgradeSprite;
}
