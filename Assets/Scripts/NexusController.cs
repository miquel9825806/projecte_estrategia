using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class NexusController : MonoBehaviour
{
    public float MaxHP;
    public float HP;
    public Image BarraVida;
    void Start()
    {
        HP = MaxHP;
    }
    void Update() {
        BarraVida.fillAmount = HP / MaxHP;
    }
    public void TakeDamage(int Damage)
    {
        HP -= Damage;
        if(HP <= 0) 
        {
            SceneManager.LoadScene("DeathParade");
        }
    }
}
