using UnityEngine;

public class ExitGame : MonoBehaviour
{
    public void StopGame()
    {
        Debug.Log("Parant el joc...");
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }
}