using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    public Transform Player;
    public int LimitTopX;
    public int LimitBottomX;
    public int LimitTopY;
    public int LimitBottomY;
    void Update()
    {
        if(Player.position.x >= LimitTopX)
        {
            this.transform.position = new Vector3(LimitTopX, this.transform.position.y, this.transform.position.z);
        }
        else if(Player.position.x <= LimitBottomX)
        {
            this.transform.position = new Vector3(LimitBottomX, this.transform.position.y, this.transform.position.z);
        }
        else
        {
            this.transform.position = new Vector3(Player.transform.position.x, this.transform.position.y, this.transform.position.z);
        }

        if (Player.position.y >= LimitTopY)
        {
            this.transform.position = new Vector3(this.transform.position.x, LimitTopY, this.transform.position.z);
        }
        else if(Player.position.y <= LimitBottomY)
        {
            this.transform.position = new Vector3(this.transform.position.x, LimitBottomY, this.transform.position.z);
        }
        else
        {
            this.transform.position = new Vector3(this.transform.position.x, Player.transform.position.y, this.transform.position.z);
        }
    }
}
