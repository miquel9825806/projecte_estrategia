using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class PointManager : MonoBehaviour
{
    public Puntuation points; // Referencia al ScriptableObject que contiene la puntuación y la ola
    public TextMeshProUGUI scoreText; // Referencia al componente TextMeshProUGUI para la puntuación
    void Start()
    {
        UpdateScoreUI(); // Inicializa el texto con la puntuación y la ola actual
    }

    void Update()
    {
        UpdateScoreUI(); // Actualiza el texto con la puntuación y la ola actual
    }

    void UpdateScoreUI()
    {
        scoreText.text = "Total Points: " + points.Points; // Actualiza el texto de la puntuación
            }

    public void AddPoints(int amount)
    {
        points.Points += amount; // Añade puntos
        UpdateScoreUI(); // Actualiza la UI
    }
    public void ResetPoints()
    {
        points.Points = 0; // Resetea los puntos
        UpdateScoreUI(); // Actualiza la UI
    }
}
