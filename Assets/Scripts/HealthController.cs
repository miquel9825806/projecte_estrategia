using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HealthController : MonoBehaviour
{
    public GameObject Player;
    private float Health;
    private float MaxHealth;

    void Start()
    {
        Player.GetComponent<PlayerController>().Movinghealth += ReloadHP;
    }

    public void ReloadHP()
    {
        MaxHealth = Player.GetComponent<PlayerController>().MaxHP;
        Health = Player.GetComponent<PlayerController>().HP;
        this.GetComponent<TextMeshProUGUI>().text = Health + " / " + MaxHealth;
    }
}
