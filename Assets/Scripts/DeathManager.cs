using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DeathManager : MonoBehaviour
{

    public GameObject Score;
    public GameObject Score2;
    public Puntuation Points;

    void Start()
    {
        Score.GetComponent<TextMeshProUGUI>().text = "Wave: " + Points.Wave;
        Score2.GetComponent<TextMeshProUGUI>().text = "Points: " + Points.Points;
    }
}
