using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    public Puntuation Object;
    public GameObject WaveControll;
    private int CurrentWave = 0;
    private int EnemiesToSpawn = 1;
    public GameObject Enemy;
    public GameObject Enemy2;
    private int EnemyMaxRange = 1;
    public List<Enemy> Enemies;
    public List<int> positions;

    void Start()
    {
        Physics2D.IgnoreLayerCollision(4, 1);
        Object.Points = 0;
        Object.Wave = 1;
    }

    void Update()
    {
        if(CurrentWave < Object.Wave)
        {
            CurrentWave = Object.Wave;
            WaveControll.GetComponent<TextMeshProUGUI>().text = "Wave: "+ CurrentWave;
            if (CurrentWave%5 == 0)
            {
                EnemiesToSpawn++;
            }
            if(CurrentWave%5 == 0 && EnemyMaxRange < Enemies.Count)
            {
                EnemyMaxRange++;
            }
            StartCoroutine(Spawn());
        }
    }

    IEnumerator Spawn(){
    yield return new WaitForSeconds(1f);
        for(int i = 0; i < EnemiesToSpawn; i++)
        {
            int position = positions[Random.Range(0, positions.Count)];
            int Spawned = Random.Range(0, EnemyMaxRange);
            if (position == 22 || position == -22)
            {
            	if(Spawned > 3){
            		GameObject EnemySpawned = Instantiate(Enemy2, new Vector3(position, Random.Range(-18, 18), 0), this.transform.rotation);
                	EnemySpawned.GetComponent<EnemyClass>().Object = Enemies[Spawned];
            	}else{
                	GameObject EnemySpawned = Instantiate(Enemy, new Vector3(position, Random.Range(-18, 18), 0), this.transform.rotation);
                	EnemySpawned.GetComponent<EnemyClass>().Object = Enemies[Spawned];
            	}
            }
            else
            {
            	if(Spawned > 3){
            		GameObject EnemySpawned = Instantiate(Enemy2 , new Vector3(Random.Range(-22, 22), position, 0), this.transform.rotation);
                	EnemySpawned.GetComponent<EnemyClass>().Object = Enemies[Spawned];
            	}else{
            		GameObject EnemySpawned = Instantiate(Enemy, new Vector3(Random.Range(-22, 22), position, 0), this.transform.rotation);
                	EnemySpawned.GetComponent<EnemyClass>().Object = Enemies[Spawned];
            	}
            }
            yield return new WaitForSeconds(0.5f);
        }
        yield return new WaitForSeconds((CurrentWave/2));
        Object.Wave++;
    }
}
