using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartGameManager : MonoBehaviour
{
    public void GoToGame()
    {
        SceneManager.LoadScene("GameScene");
        Time.timeScale = 1;
    }
}
