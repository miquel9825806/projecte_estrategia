using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class PlayerBaseAttack : MonoBehaviour
{
    public GameObject Wave;
    private Vector2 Mouse;
    private Vector2 PosPlayer;
    public int AttackWaves;
    private int AttackWavesCount = 0;
    private float Distance = 0.5f;
    private float scale = 1.5f;

    void Start()
    {
        GameObject[] Player = (GameObject.FindGameObjectsWithTag("Player"));
        AttackWaves = Player[0].GetComponent<PlayerController>().AttackWaves;
        Mouse = Camera.main.ScreenToViewportPoint(Input.mousePosition);
        PosPlayer = Camera.main.WorldToViewportPoint(transform.position);
        float angle = Mathf.Atan2(PosPlayer.y - Mouse.y, PosPlayer.x - Mouse.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle + 90f));
        transform.position += transform.up * 0.75f;
        StartCoroutine(Waves());
    }

    IEnumerator Waves()
    {
        for(int i = 0; i < AttackWaves; AttackWaves--){
            transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
            AttackWavesCount++;
            GameObject Wavespawned = Instantiate(Wave, this.transform.position + new Vector3(0, Distance, 0), this.transform.rotation, this.transform);
            Wavespawned.transform.localScale = new Vector3(scale, scale, 0);
            scale += 0.5f;
            Distance += 0.20f + (0.15f * AttackWavesCount);
            
            float angle = Mathf.Atan2(PosPlayer.y - Mouse.y, PosPlayer.x - Mouse.x) * Mathf.Rad2Deg;
            transform.rotation = Quaternion.Euler(new Vector3(0f, 0f, angle + 90f));
            yield return new WaitForSeconds(0.1f);
        }
        yield return new WaitForSeconds(0.25f);
        Destroy(this.gameObject);
    }
}
