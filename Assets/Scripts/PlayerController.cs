using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D Rb;
    public Transform SpawnPoint;
    public Transform Sprite;
    public GameObject BasicAttack;
    public Transform ProjectileSpawner;
    public GameObject HealthUI;
    public GameObject GoldUI;
    public int Gold;
    public delegate void Delegate();
    public event Delegate AddingGold;
    public delegate void Delegate2();
    public event Delegate Movinghealth;

    public int MaxSpeed;
    public int Speed;
    public int MaxHP;
    public int HP;
    public int MaxDmg;
    public int Dmg;
    public int AttackWaves;
    public float MaxAttackCD;
    public float AttackCD;
    public int CritChance;
    public float CritDamage;

    private bool CanAttack = true;
    private float RespawnTime = 1.5f;
    private bool Alive = true;

    [SerializeField] public AudioClip goldSound;
    [SerializeField] public AudioClip magicSound;
    private AudioSource audioSource;
    public bool shouldPlayGoldSound = true;

    void Start()
    {
        Rb = GetComponent<Rigidbody2D>();
        HP = MaxHP;
        HealthUI.GetComponent<HealthController>().ReloadHP();
        Speed = MaxSpeed;
        Dmg = MaxDmg;
        AttackCD = MaxAttackCD;
        Gold = 0;
        audioSource = GetComponent<AudioSource>();
        if (audioSource == null)
        {
            audioSource = gameObject.AddComponent<AudioSource>();
        }
        audioSource.playOnAwake = false;
        audioSource.clip = goldSound;
        audioSource.Stop();
    }

    void Update()
    {
        if (Alive && Time.timeScale != 0)
        {
            Vector3 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            mousePosition.z = 0f;

            Vector2 direction = (mousePosition - transform.position).normalized;

            Rb.velocity = direction * Speed;

            Vector3 difference = mousePosition - transform.position;
            difference.Normalize();
            float rotationZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg;
            Sprite.rotation = Quaternion.Euler(0f, 0f, rotationZ);

            // Player Attack
            if (Input.GetMouseButtonDown(0) && CanAttack)
            {
                Instantiate(BasicAttack, ProjectileSpawner.position, Quaternion.Euler(0f, 0f, rotationZ));
                StartCoroutine(PlayerAttackCD());
                audioSource.PlayOneShot(magicSound);
            }
        }
        else
        {
            Rb.velocity = Vector2.zero;
        }
    }

    public int AttackDamage()
    {
        int DealtDamage = Dmg;
        if (Random.Range(1, 102) <= CritChance)
        {
            DealtDamage = (int)(DealtDamage * CritDamage);
        }
        return DealtDamage;
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("Potion"))
        {
            HealHP(2);
            Destroy(col.gameObject);
        }
        else if (col.CompareTag("1 Gold"))
        {
            AddGold(1);
            if (shouldPlayGoldSound)
        {
            audioSource.PlayOneShot(goldSound);
        }
            Destroy(col.gameObject);
        }
        else if (col.CompareTag("5 Gold"))
        {
            AddGold(5);
            if (shouldPlayGoldSound)
        {
            audioSource.PlayOneShot(goldSound);
        }
            Destroy(col.gameObject);
        }
        else if (col.CompareTag("10 Gold"))
        {
            AddGold(10);
            if (shouldPlayGoldSound)
        {
            audioSource.PlayOneShot(goldSound);
        }
            Destroy(col.gameObject);
        }
    }

    public void HealHP(int HealHP)
    {
        if (HealHP == 0)
        {
            HealHP = MaxHP;
        }
        if ((HP + HealHP) <= MaxHP)
        {
            HP += HealHP;
        }
        else
        {
            HP = MaxHP;
        }
        if (HP <= 0)
        {
            HP = 0;
            StartCoroutine(PlayerDeath());
        }
        Movinghealth?.Invoke();
    }

    public void AddGold(int GoldIncrease)
    {
        Gold += GoldIncrease;
        AddingGold?.Invoke();
    }

    IEnumerator PlayerAttackCD()
    {
        CanAttack = false;
        yield return new WaitForSeconds(AttackCD);
        CanAttack = true;
    }

    IEnumerator PlayerDeath()
    {
        Alive = false;
        Rb.velocity = Vector2.zero;
        Rb.isKinematic = true;
        yield return new WaitForSeconds(RespawnTime);
        transform.position = SpawnPoint.position;
        Rb.isKinematic = false;
        Rb.velocity = Vector2.zero;
        Alive = true;
        HealHP(MaxHP);
    }
}
